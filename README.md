# Design

CSS design stack of Nexylan.

## Requirements

- NodeJS

## Usage

### CDN

```html
<link rel="stylesheet" href="https://design.nexylan.dev/app.css" />
```

### NodeJS dependency

Specify the right registry for any `@nexylan` package and setup an access token if not already done:

```
npm config set @nexylan:registry https://gitlab.com/api/v4/packages/npm/
npm config set '//gitlab.com/api/v4/packages/npm/:_authToken' "<your_token>"
```

More info on the [GitLab documentation](https://docs.gitlab.com/ee/user/packages/npm_registry/index.html#authenticating-to-the-gitlab-npm-registry).

Run the following install command:

```bash
npm install @nexylan/design
```

Then add the `@nexylan/design` configuration to your `tailwind.config.js`:

#### Tailwind configuration

```javascript
module.exports = {
  presets: [require("tailwindcss/defaultConfig"), require("@nexylan/design/tailwind.config.js")],
  // Rest of your configuration.
};
```

#### Colors import

```javascript
import { colors } from "@nexylan/design";

console.log(colors.primary);
```

See [src/colors.js](src/colors.js) for details.

### Docker

```Dockerfile
FROM whatever
COPY --from=registry.gitlab.com/nexylan/design /usr/share/nginx/html/app.css /path/to/target
```

### Fonts

To take all benefits of the Nexylan design, you have to import some fonts:

```html
<link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin />
<!-- Inter -->
<link
  href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap"
  rel="stylesheet"
/>
<!-- Fira Code -->
<link href="https://fonts.googleapis.com/css?family=Fira+Code&display=swap&subset=latin-ext" rel="stylesheet" />
```

### Favicons

```html
<link rel="icon" type="image/png" href="https://design.nexylan.dev/favicon.png" />
<link rel="icon" type="image/x-icon" href="https://design.nexylan.dev/favicon.ico" />
```

This is the simplest recommended way.
See also: https://medium.com/clio-calliope/making-google-fonts-faster-aadf3c02a36d

## Development

```
make
```

### Favicon generation

- Convert icon.svg to favicon.png using gimp:
  - Open the svg image.
  - Resize the image to have a maximum of 128px.
  - Right click -> Image -> Canvas size.
  - Export the image as favicon.png
- Convert favicon.png to favicon.icon:
  - `convert static/favicon.png static/favicon.ico`

Note: It would be better to have a script integrated to the Makefile.
I don't know how to setup properly yet, any contribution is welcomed.
