/* eslint-disable global-require */
const gulp = require('gulp');
const postcss = require('gulp-postcss');

gulp.task('default', () => gulp.src('./src/*.css')
  .pipe(postcss())
  .pipe(gulp.dest('./static')));
