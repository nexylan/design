module.exports = ({ addComponents, theme }) => addComponents({
  '.input': {
    padding: theme('padding.3'),
    margin: theme('margin.3'),
    color: 'currentColor',
    backgroundColor: theme('colors.grayLight'),
    borderStyle: theme('border.solid'),
    borderWidth: theme('borderWidth.default'),
    borderColor: theme('colors.gray.500'),
    borderRadius: theme('borderRadius.default'),
    transitionProperty: theme('transitionProperty.default'),
    transitionDuration: theme('transitionDuration.200'),
    '&::placeholder': {
      color: theme('colors.black'),
    },
    '&:focus': {
      boxShadow: theme('boxShadow.focus'),
    },
    '&:focus.border-primary': {
      boxShadow: theme('boxShadow.focus-primary'),
    },
    '&:focus.border-success': {
      boxShadow: theme('boxShadow.focus-success'),
    },
    '&:focus.border-warning': {
      boxShadow: theme('boxShadow.focus-warning'),
    },
    '&:focus.border-info': {
      boxShadow: theme('boxShadow.focus-info'),
    },
    '&:focus.border-danger': {
      boxShadow: theme('boxShadow.focus-danger'),
    },
    '&:focus.border-muted': {
      boxShadow: theme('boxShadow.focus-muted'),
    },
  },
});
