module.exports = ({ addComponents, theme }) => addComponents({
  '.btn': {
    display: 'inline-block',
    padding: theme('padding.3'),
    margin: theme('margin.3'),
    color: 'currentColor',
    fontSize: theme('text.fontSize.lg'),
    backgroundColor: theme('colors.white'),
    borderStyle: theme('border.solid'),
    borderWidth: theme('borderWidth.default'),
    borderColor: theme('colors.gray.300'),
    borderRadius: theme('borderRadius.default'),
    fontWeight: theme('fontWeight.semibold'),
    transitionProperty: theme('transitionProperty.default'),
    transitionDuration: theme('transitionDuration.200'),
    '&:hover': {
      backgroundColor: theme('colors.gray.700'),
      borderColor: theme('colors.gray.700'),
      color: theme('colors.white'),
    },
  },
  '.btn-primary': {
    color: theme('colors.white'),
    borderColor: theme('colors.primary'),
    backgroundColor: theme('colors.primary'),
    '&:hover': {
      borderColor: theme('colors.green.500'),
      backgroundColor: theme('colors.green.500'),
    },
  },
  '.btn-success': {
    color: theme('colors.white'),
    borderColor: theme('colors.success'),
    backgroundColor: theme('colors.success'),
    '&:hover': {
      borderColor: theme('colors.green.700'),
      backgroundColor: theme('colors.green.700'),
    },
  },
  '.btn-warning': {
    color: theme('colors.white'),
    borderColor: theme('colors.warning'),
    backgroundColor: theme('colors.warning'),
    '&:hover': {
      borderColor: theme('colors.orange.700'),
      backgroundColor: theme('colors.orange.700'),
    },
  },
  '.btn-danger': {
    color: theme('colors.danger'),
    borderColor: theme('colors.danger'),
    backgroundColor: theme('colors.white'),
    '&:hover': {
      colors: theme('colors.white'),
      borderColor: theme('colors.danger'),
      backgroundColor: theme('colors.danger'),
    },
  },
  '.btn-info': {
    color: theme('colors.white'),
    borderColor: theme('colors.info'),
    backgroundColor: theme('colors.info'),
    '&:hover': {
      borderColor: theme('colors.blue.700'),
      backgroundColor: theme('colors.blue.700'),
    },
  },
  '.btn-muted': {
    color: theme('colors.white'),
    borderColor: theme('colors.muted'),
    backgroundColor: theme('colors.muted'),
    '&:hover': {
      borderColor: theme('colors.gray.300'),
      backgroundColor: theme('colors.gray.300'),
    },
  },
  '.btn-link': {
    color: theme('colors.gray.900'),
    borderColor: 'transparent',
    backgroundColor: 'transparent',
    '&:hover': {
      color: theme('colors.primary'),
      borderColor: 'transparent',
      backgroundColor: 'transparent',
    },
  },
});
