module.exports = ({ addBase, theme }) => addBase({
  body: {
    fontFamily: theme('fontFamily.sans').join(', '),
    color: '#111111',
  },
  '::selection,::-moz-selection': {
    color: theme('colors.white'),
    backgroundColor: theme('colors.primary'),
  },
  'h1,h2,h3,h4,h5,h6': {
    fontWeight: theme('fontWeight.semibold'),
  },
  a: {
    color: theme('colors.primary'),
    '&:hover': {
      color: theme('colors.purple.700'),
    },
  },
});
