const colors = {
  blue: '#4299e1',
  gray: '#8e8d95',
  green: '#48d3aa',
  orange: '#ff8d00',
  red: '#f56565',
  pink: '#ffabb7',
  purple: '#5f4ae3',
  teal: '#0b7d89',
  yellow: '#ffe000',
};

module.exports = {
  ...colors,
  primary: colors.purple,
  success: colors.green,
  warning: colors.orange,
  info: colors.blue,
  danger: colors.red,
  muted: colors.gray,
};
