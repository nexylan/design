#!/usr/bin/env bash
set -e

for image in static/img/logo/*.svg; do
	echo -n "convert: ${image} to PNG format..."
	http --form POST https://imagine.nexylan.dev/convert \
		stripmeta==true \
		lossless==true \
		type==png \
		"file@${image}" \
		>"${image%.*}.png"
	echo " [OK]"
done
