import '../src/app.css';

const config = require('../tailwind.config.js');

export default {
  title: 'Typography',
};

const makeWrapper = () => {
  const wrapper = document.createElement('div');

  wrapper.classList.add('px-4');

  return wrapper;
};

export const Titles = () => {
  const div = makeWrapper();

  for (let level = 1; level <= 6; level++) {
    const title = document.createElement(`h${level}`);

    title.innerText = `Level ${level} title`;

    div.appendChild(title);
  }

  return div;
};

export const Sizes = () => {
  const div = makeWrapper();

  Object.keys(config.theme.fontSize).forEach((size) => {
    const text = document.createElement('div');
    const cssClass = `text-${size}`;

    text.innerText = cssClass;
    text.classList.add(cssClass);

    div.appendChild(text);
  });

  return div;
};

export const Links = () => {
  const div = makeWrapper();

  const p = document.createElement('p');
  p.innerHTML = 'This is a <a href="#">link</a>.';
  div.appendChild(p);

  const title = document.createElement('h1');
  title.innerHTML = 'There is a <a href="#">link</a> on this title.';
  div.appendChild(title);

  const imgWrapper = document.createElement('div');
  const imgDesc = document.createElement('p');
  imgDesc.innerHTML = 'The following image is a link:';
  imgWrapper.appendChild(imgDesc);
  const imgLink = document.createElement('a');
  imgLink.href = '#';
  const img = document.createElement('img');
  img.setAttribute('src', 'https://www.w3schools.com/tags/logo_w3s.gif');
  imgLink.appendChild(img);
  imgWrapper.appendChild(imgLink);
  div.appendChild(imgWrapper);

  return div;
};

const makeList = (styleClass, positionClass) => {
  const wrapper = document.createElement('div');
  wrapper.classList.add('m-3');
  const classString = document.createElement('p');
  classString.innerHTML = `.${styleClass}.${positionClass}`;
  wrapper.appendChild(classString);

  const list = document.createElement(styleClass === 'list-decimal' ? 'ol' : 'ul');
  list.classList.add(styleClass, positionClass);

  for (let i = 0; i < 6; ++i) {
    const element = document.createElement('li');
    element.innerHTML = 'List element';
    list.appendChild(element);
  }
  wrapper.appendChild(list);

  return wrapper;
};

export const Lists = (args) => {
  const div = makeWrapper();

  const positionClass = args.outsidePosition ? 'list-outside' : 'list-inside';

  div.appendChild(makeList('list-disc', positionClass));
  div.appendChild(makeList('list-decimal', positionClass));
  div.appendChild(makeList('list-none', positionClass));

  return div;
};
Lists.args = {
  outsidePosition: true,
};
