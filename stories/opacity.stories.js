import '../src/app.css';

const config = require('../tailwind.config.js');

export default {
  title: 'Opacity',
};

export const Opacity = () => {
  const div = document.createElement('div');
  div.classList.add('p-4');

  Object.keys(config.theme.opacity).forEach((name) => {
    const className = `opacity-${name}`;
    const opacityDiv = document.createElement('div');
    opacityDiv.className = `bg-blue-500 my-2 text-center ${className}`;

    const opacityTitle = document.createElement('h3');
    opacityTitle.className = 'm-auto text-white';
    opacityTitle.innerText = className;
    opacityDiv.appendChild(opacityTitle);

    div.appendChild(opacityDiv);
  });

  return div;
};
