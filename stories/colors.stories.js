import '../src/app.css';

const config = require('../tailwind.config.js');

delete config.theme.colors.transparent;
const colors = Object.entries(config.theme.colors).map(([name, color]) => {
  // Transform single value to object to make parsing easier.
  if (typeof color === 'string') {
    return [name, {
      base: color,
    }];
  }

  return [name, color];
}).sort(
  // Put single color options on top of the list.
  ([, colorA], [, colorB]) => Object.keys(colorA).length - Object.keys(colorB).length,
);

export default {
  title: 'Colors',
};

export const Colors = () => {
  const div = document.createElement('div');

  div.classList.add(...['flex', 'flex-wrap', 'p-4']);

  colors.forEach(([name, color]) => {
    const isUnique = Object.keys(color).length === 1;
    const colorDiv = document.createElement('div');
    colorDiv.classList.add(...[
      'py-2',
      'relative',
      ...isUnique ? ['w-1/2', 'md:w-1/3'] : ['w-full'],
    ]);

    const colorTitle = document.createElement('h3');
    colorTitle.innerText = name;
    colorDiv.appendChild(colorTitle);

    const colorVariants = document.createElement('div');
    colorVariants.classList.add(...['flex', 'flex-wrap']);
    Object.entries(color).forEach(([variant, code]) => {
      const baseClassName = variant !== 'base' ? `${name}-${variant}` : name;
      const variantDiv = document.createElement('div');
      variantDiv.classList.add(...[
        'm-2',
        ...isUnique ? ['w-full'] : ['w-1/2', 'md:w-1/3'],
      ]);

      const variantInfo = document.createElement('div');
      variantInfo.classList.add(...['flex', 'mb-2', 'items-center']);

      const variantSquare = document.createElement('div');
      variantSquare.className = `h-12 w-12 rounded-lg shadow-inner bg-${baseClassName}`;

      const variantDescriptionDiv = document.createElement('div');
      variantDescriptionDiv.className = 'ml-2 text-gray-800 text-xs leading-none pl-1';

      const variantClassDiv = document.createElement('div');
      variantClassDiv.className = 'font-semibold select-all';
      variantClassDiv.innerText = baseClassName;
      variantDescriptionDiv.appendChild(variantClassDiv);

      const variantColorDiv = document.createElement('div');
      variantColorDiv.className = 'mt-1 font-normal opacity-75 select-all';
      variantColorDiv.innerText = code;
      variantDescriptionDiv.appendChild(variantColorDiv);

      variantInfo.appendChild(variantSquare);
      variantInfo.appendChild(variantDescriptionDiv);

      const variantExampleDiv = document.createElement('div');
      variantExampleDiv.classList.add(...[`text-${baseClassName}`, 'flex-1']);

      const variantExampleText = document.createElement('p');
      variantExampleText.innerText = 'Text example with the color.';
      variantExampleDiv.appendChild(variantExampleText);

      variantDiv.appendChild(variantInfo);
      variantDiv.appendChild(variantExampleDiv);
      colorVariants.appendChild(variantDiv);
    });
    colorDiv.appendChild(colorVariants);

    div.appendChild(colorDiv);
  });

  return div;
};
