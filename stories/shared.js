export const argTypes = {
  color: {
    name: 'Color',
    defaultValue: 'default',
    control: {
      type: 'inline-radio',
      options: [
        'default',
        'primary',
        'success',
        'warning',
        'info',
        'danger',
        'muted',
      ],
    },
  },
};

export default null;
