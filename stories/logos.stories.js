import '../src/app.css';

export default {
  title: 'Logos',
};

const createLogoDownloadLink = (baseHref, extension) => {
  const link = document.createElement('a');
  link.className = 'mx-1';
  link.innerText = extension.toUpperCase();
  link.href = `${baseHref}.${extension}`;

  return link;
};

export const Logos = () => {
  const logos = process.env.LOGOS
    .map((logo) => logo.split('.').slice(0, -1).join('.'))
    // @see https://stackoverflow.com/a/14438954/1731473
    .filter((v, i, a) => a.indexOf(v) === i);
  const div = document.createElement('div');

  div.className = 'flex flex-wrap';

  logos.forEach((filename) => {
    const logoHref = `img/logo/${filename}`;

    const logoElement = document.createElement('div');
    logoElement.className = 'flex-1 text-center m-4';

    const logoFigure = document.createElement('figure');

    const logoImg = document.createElement('img');
    logoImg.className = 'p-2 h-32 lg:h-64 mx-auto';
    logoImg.src = `${logoHref}.svg`;
    logoImg.alt = filename;

    const logoCaption = document.createElement('figcaption');
    logoCaption.innerText = filename;
    logoFigure.appendChild(logoImg);
    logoFigure.appendChild(logoCaption);

    const links = document.createElement('div');
    links.appendChild(createLogoDownloadLink(logoHref, 'svg'));
    links.appendChild(createLogoDownloadLink(logoHref, 'png'));

    logoElement.appendChild(logoFigure);
    logoElement.appendChild(links);
    div.appendChild(logoElement);
  });

  return div;
};

export const Favicons = () => `
<ul class="p-4">
  <li>
    <a href="favicon.png">
      <img src="/favicon.png" alt="favicon" width="16" class="inline" /> favicon.png
    </a>
  </li>
  <li>
    <a href="favicon.ico">
      <img src="/favicon.ico" alt="favicon" width="16" class="inline" /> favicon.ico
    </a>
  </li>
</ul>
`;
