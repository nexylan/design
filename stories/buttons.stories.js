import '../src/app.css';
import { argTypes } from './shared';

export default {
  title: 'Buttons',
  argTypes: {
    color: argTypes.color,
  },
};

const createButton = (type, color) => {
  const classes = ['btn', 'flex-1'];
  const btn = document.createElement(type);

  if (color !== 'default') {
    classes.push(`btn-${color}`);
  }

  btn.type = type;
  btn.innerText = `${color} (${classes.join(' ')})`;
  btn.classList.add(...classes);

  return btn;
};

export const Button = (args) => {
  const div = document.createElement('div');
  div.appendChild(createButton('button', args.color));
  div.appendChild(createButton('a', args.color));

  return div;
};

export const ButtonLink = () => {
  const div = document.createElement('div');
  div.appendChild(createButton('button', 'link'));
  div.appendChild(createButton('a', 'link'));

  return div;
};

export const ButtonMultiple = (args) => {
  const div = document.createElement('div');

  div.classList.add(['flex', 'flex-wrap']);
  div.appendChild(createButton('button', args.color));
  div.appendChild(createButton('button', args.color));
  div.appendChild(createButton('button', args.color));
  div.appendChild(createButton('button', args.color));
  div.appendChild(createButton('button', args.color));

  return div;
};
