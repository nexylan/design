import '../src/app.css';
import { argTypes } from './shared';

export default {
  title: 'Forms',
  argTypes: {
    color: argTypes.color,
  },
};

const makeInput = (color) => {
  const classes = ['input', 'flex-1'];
  const input = document.createElement('input');

  if (color !== 'default') {
    classes.push(`border-${color}`);
  }

  input.placeholder = `${color} (${classes.join(' ')})`;
  input.classList.add(...classes);

  return input;
};

export const Input = (args) => makeInput(args.color);

export const InputMultiple = (args) => {
  const div = document.createElement('div');
  div.classList.add([
    'flex',
    'flex-wrap',
  ]);

  div.appendChild(makeInput(args.color));
  div.appendChild(makeInput(args.color));
  div.appendChild(makeInput(args.color));
  div.appendChild(makeInput(args.color));
  div.appendChild(makeInput(args.color));

  return div;
};

const makeSelect = (color) => {
  const classes = ['input', 'flex-1'];
  const select = document.createElement('select');

  if (color !== 'default') {
    classes.push(`border-${color}`);
  }
  [
    'Option 1',
    'Option 2',
    'This is a very long option.',
    'Option 3',
  ].forEach((optionLabel) => {
    const option = document.createElement('option');
    option.innerHTML = optionLabel;

    select.appendChild(option);
  });

  select.placeholder = `${color} (${classes.join(' ')})`;
  select.classList.add(...classes);

  return select;
};

export const Select = (args) => makeSelect(args.color);

export const SelectMultiple = (args) => {
  const div = document.createElement('div');
  div.classList.add([
    'flex',
    'flex-wrap',
  ]);

  div.appendChild(makeSelect(args.color));
  div.appendChild(makeSelect(args.color));
  div.appendChild(makeSelect(args.color));
  div.appendChild(makeSelect(args.color));
  div.appendChild(makeSelect(args.color));

  return div;
};
