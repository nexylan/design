/* eslint-disable global-require */
const pSBC = require('shade-blend-color').default;
const Color = require('color');
const {
  colors, boxShadow, fontFamily, fontSize, opacity, screens,
} = require('tailwindcss/defaultTheme');
const brandColors = require('./src/colors');

delete colors.indigo;

function makeColor(color, start = 500) {
  const madeColors = {};
  madeColors[start] = color;

  for (let s = start; s <= 900; s += 100) {
    madeColors[s] = pSBC(-((s - start) / 1000), color);
  }

  for (let s = start; s >= 100; s -= 100) {
    madeColors[s] = pSBC((start - s) / 1000, color);
  }

  return madeColors;
}

let customColors = Object.assign(colors, {
  green: makeColor(brandColors.green),
  orange: makeColor(brandColors.orange),
  yellow: makeColor(brandColors.yellow),
  purple: makeColor(brandColors.purple),
  teal: makeColor(brandColors.teal),
  pink: makeColor(brandColors.pink),
  gray: makeColor(brandColors.gray),
});

const namedColors = {
  primary: brandColors.primary,
  success: brandColors.success,
  warning: brandColors.warning,
  info: brandColors.info,
  danger: brandColors.danger,
  muted: brandColors.muted,
  // Useful for light background differentiation, like inputs.
  'gray-light': '#f5f5f5',
};

customColors = Object.assign(customColors, namedColors);

const focusShadow = (color) => `0 0 0 .125rem ${Color(color).alpha(0.5).string()}`;
const focusShadows = {
  focus: focusShadow(customColors.gray[500]),
};
Object.entries(namedColors).forEach(([key, value]) => {
  focusShadows[`focus-${key}`] = focusShadow(value);
});

module.exports = {
  theme: {
    screens: {
      ...screens,
      '2xl': '1440px',
      '3xl': '1680px',
      '4xl': '2048px',
    },
    container: {
      center: true,
      padding: '1rem',
    },
    colors: customColors,
    fontFamily: {
      ...fontFamily,
      sans: ['Inter', ...fontFamily.sans],
      mono: ['Fira Code', ...fontFamily.mono],
    },
    boxShadow: {
      ...boxShadow,
      ...focusShadows,
    },
    opacity: {
      ...opacity,
      15: '.15',
    },
    fontSize: {
      xxs: '.55rem',
      ...fontSize,
    },
    extend: {
      spacing: {
        128: '32rem',
      },
    },
  },
  plugins: [
    require('./src/base'),
    require('./src/components/buttons'),
    require('./src/components/input'),
  ],
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
};
