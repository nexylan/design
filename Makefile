all: install convert up

install:
	npm install

convert:
	bash convert.sh

up:
	npm run storybook
