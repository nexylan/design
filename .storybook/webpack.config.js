/* eslint-disable global-require */
const webpack = require('webpack');
const path = require('path');
const fs = require('fs');

module.exports = async ({ config }) => {
  config.plugins.push(
    new webpack.DefinePlugin({
      'process.env.LOGOS': JSON.stringify(fs.readdirSync('./static/img/logo')),
    }),
  );

  config.module.rules.push({
    test: /\.css$/,
    use: [
      {
        loader: 'postcss-loader',
      },
    ],
    include: path.resolve(__dirname, '../'),
  });
  return config;
};
